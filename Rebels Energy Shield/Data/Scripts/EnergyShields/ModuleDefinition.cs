﻿using System;

namespace Cython.EnergyShields
{
	public class ModuleDefinition
	{
		public float PointMultiplier = 1f;
		public float RechargeMultiplier = 1f;
		public float PowerMultiplier = 5.5f;

		public ModuleDefinition ()
		{
			
		}
	}
}

